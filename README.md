# JS_DRILLS_OBJECTS

###  Introduction of the project aim

```js
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.
// Check and use MDN as well

/*
    Create a function for each problem in a file called
        keys.js
        values.js
        pairs.js
    and so on in the root of the project.

    Ensure that the functions in each file is exported and tested in its own file called
        testKeys.js
        testValues.js
        testPairs.js
    and so on in a folder called test.

    Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project.
    Ensure that the repo is a public repo.

    When you are done, send the gitlab url to your mentor
*/

function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
}

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
}

function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
}

function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}

/* STRETCH PROBLEMS */

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    }

function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
}
```

### Technologies Used

1. **Testing Framework**: Jest
  - Jest is a JavaScript testing framework with a focus on simplicity and efficiency. It is widely used for writing unit tests, ensuring the reliability and correctness of your code.
2. **Package Manager**: npm (Node Package Manager)
   - npm is the default package manager for Node.js, allowing you to easily manage and install dependencies for your projects. It simplifies the process of sharing and distributing code packages.
3. **Programming Language**: JavaScript (JS)
    - JavaScript is a versatile and widely-used programming language. In your project, JavaScript is likely used for both front-end and back-end development, providing the interactivity and functionality of your application.


### Table of Contents
- [JS\_DRILLS\_OBJECTS](#js_drills_objects)
    - [Introduction of the project aim](#introduction-of-the-project-aim)
    - [Technologies Used](#technologies-used)
    - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
- [Navigate to the project directory](#navigate-to-the-project-directory)
- [Install dependencies](#install-dependencies)
    - [Testing](#testing)
  - [Testing the solutions](#testing-the-solutions)


## Installation

Describe's the steps to install the project and its dependencies.

```bash
# Clone the repository
git clone git@gitlab.com:Chandu_Malla/js_drills_objects.git
```

# Navigate to the project directory
```
cd js_drill_objects
```

# Install dependencies
```
npm install
```

### Testing

Each test file in the test folder tested indivdually with custom test 
cases.

### Testing the solutions

Go through the test directory then run node command on each test

```
cd test/
```

By running the below command, which executes custom test cases & returns output, we will use jest to test the code.

```
# To run each file
npm test testKeys.js
```

```
# To run all files at once
npm test
```
