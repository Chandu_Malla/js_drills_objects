const defaults = (obj, defaultProp) => {

    if (obj.constructor !== Object || defaultProp.constructor !== Object) {
        throw new TypeError('Invalid arguments. Objects expected.');
    }

    for (let key in defaultProp) {
        if (!obj.hasOwnProperty(key) && !obj[key]) {
            obj[key] = defaultProp[key];
        }
    }

    return obj;
};

module.exports = defaults