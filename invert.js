const invert = (obj) => {

    const invertedObject = {};

    if (obj.constructor === Object) {
        for (let key in obj) {
            invertedObject[obj[key]] = key;
        }
        return invertedObject;
    } else {
        throw new TypeError('Invalid argument. Object expected.');
    }

};

module.exports = invert;