const keys = (testObject) => {
    const objectKeys = [];

    if (testObject.constructor === Object) {
        for (let key in testObject) {
            objectKeys.push(key);
        }
        return objectKeys;
    } else {
        throw new TypeError('Invalid argument. Object expected.');
    }
    
};

module.exports = keys
