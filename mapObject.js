const mapObject = (testObject, callback) => {
    const map = {}

    if (testObject.constructor === Object) {
        for (let key in testObject) {
            map[key] = callback(testObject[key], key, testObject)
        }

        return map
    } else {
        throw new TypeError('Invalid argument. Object expected.')
    }

}

module.exports = mapObject