const pairs = (obj) => {
    const twins = [];

    if (obj.constructor === Object) {
        for (let key in obj) {
            twins.push([key, obj[key]]);
        }

        return twins;
    } else {
        throw new TypeError('Invalid argument. Object expected.');
    }
};

module.exports = pairs
