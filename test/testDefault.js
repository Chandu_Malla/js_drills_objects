const defaults = require('../defaults');

describe('defaults function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    
    test('returns the object with default properties added', () => {
        const defaultProperties = { age: 40, occupation: 'Superhero' };
        const result = defaults(testObject, defaultProperties);
        
        expect(result).toEqual({
            name: 'Bruce Wayne',
            age: 36,
            location: 'Gotham',
            occupation: 'Superhero',
        });
    });

    test('throws TypeError for non-object arguments', () => {
        const invalidObject = 'not an object';
        const defaultProperties = { age: 40, occupation: 'Superhero' };

        expect(() => defaults(invalidObject, defaultProperties)).toThrowError('Invalid arguments. Objects expected.');
        expect(() => defaults(testObject, 'not an object')).toThrowError('Invalid arguments. Objects expected.');
    });

    test('handles empty object', () => {
        const emptyObject = {};
        const defaultProperties = { age: 40, occupation: 'Superhero' };
        const result = defaults(emptyObject, defaultProperties);

        expect(result).toEqual({ age: 40, occupation: 'Superhero' });
    });

});
