const invert = require('../invert');

describe('invert function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    test('returns an inverted object', () => {
        const result = invert(testObject);

        expect(result).toEqual({
            'Bruce Wayne': 'name',
            '36': 'age',
            'Gotham': 'location',
        });
    });

    test('throws TypeError for non-object arguments', () => {
        const invalidObject = 'not an object';

        expect(() => invert(invalidObject)).toThrowError('Invalid argument. Object expected.');
    });

    test('handles empty object', () => {
        const emptyObject = {};

        const result = invert(emptyObject);

        expect(result).toEqual({});
    });
});
