const keys = require('../keys');

describe('keys function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    test('returns an array of keys for a valid object', () => {
        expect(keys(testObject)).toEqual(['name', 'age', 'location']);
    });

    test('throws TypeError for non-object argument', () => {
        expect(() => keys('not an object')).toThrowError('Invalid argument. Object expected.');
    });

    test('handles empty object', () => {
        expect(keys({})).toEqual([]);
    });
});