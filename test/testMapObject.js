const mapObject = require('../mapObject');

describe('mapObject function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    test('maps values using the provided callback for a valid object', () => {
        const callback = (value, key) => `${key}: ${value}`;
        expect(mapObject(testObject, callback)).toEqual({
            name: 'name: Bruce Wayne',
            age: 'age: 36',
            location: 'location: Gotham',
        });
    });

    test('throws TypeError for non-object argument', () => {
        const callback = jest.fn();
        expect(() => mapObject('not an object', callback)).toThrowError('Invalid argument. Object expected.');
        expect(callback).not.toHaveBeenCalled();
    });

    test('handles empty object', () => {
        const callback = jest.fn();
        expect(mapObject({}, callback)).toEqual({});
        expect(callback).not.toHaveBeenCalled();
    });
});
