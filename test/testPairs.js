const pairs = require('../pairs');

describe('pairs function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    test('returns an array of key-value pairs for a valid object', () => {
        expect(pairs(testObject)).toEqual([
            ['name', 'Bruce Wayne'],
            ['age', 36],
            ['location', 'Gotham'],
        ]);
    });

    test('throws TypeError for non-object argument', () => {
        expect(() => pairs('not an object')).toThrowError('Invalid argument. Object expected.');
    });

    test('handles empty object', () => {
        expect(pairs({})).toEqual([]);
    });

    
});
