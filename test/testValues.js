const values = require('../values')

describe('values function', () => {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    test('returns an array of values for a valid object', () => {
        expect(values(testObject)).toEqual(['Bruce Wayne', 36, 'Gotham']);
    });

    test('throws TypeError for non-object argument', () => {
        expect(() => values('not an object')).toThrowError('Invalid argument. Object expected.');
    });

    test('handles empty object', () => {
        expect(values({})).toEqual([]);
    });

});
