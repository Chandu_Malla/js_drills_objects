const values = (testObject) => {
    const objectValues = []

    if (testObject.constructor === Object) {
        for (let key in testObject) {
            objectValues.push(testObject[key])
        }
        return objectValues
    } else {
        throw new TypeError('Invalid argument. Object expected.')
    }
}

module.exports = values
